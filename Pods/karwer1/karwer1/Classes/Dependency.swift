public class Dependency {

    public init() {
        
    }
    
    public func test1() {
        print("This function works in 1.0")
    }

    public func test2() {
        fatalError("This function does not work in 1.0")
    }
    
}
